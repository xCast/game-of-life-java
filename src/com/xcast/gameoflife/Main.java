package com.xcast.gameoflife;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException
    {
        GameOfLife gameOfLife = new GameOfLife(30, 30);
        gameOfLife.startPopulation(
                new Point(5,5),
                new Point(5,6),
                new Point(6,5),
                new Point(5,4),
                new Point(4,5),
                new Point(4,4),
                new Point(4,6),
                new Point(3,5),
                new Point(8,4),
                new Point(5,3),
                new Point(7,4),
                new Point(5,8)

        );
        int x = 1000;
        while(gameOfLife.getAliveCount()>0 && !gameOfLife.isStable())
        {
            clearScreen();
            gameOfLife.print();
            gameOfLife.iterate();
            waitMs(x);
            if(x>20)x-=140;
        }
    }

    private static void waitMs(long ms)
    {
        if(ms<1) {return;}
        try
        {
            Thread.sleep(ms);
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    public static void clearScreen() {
        try
        {
            final String os = System.getProperty("os.name");

            if (os.contains("Windows"))
            {
                //Runtime.getRuntime().exec("cls");
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            }
            else
            {
                Runtime.getRuntime().exec("clear");
            }
        }
        catch (final Exception e)
        {
            //  Handle any exceptions.
        }
    }
}
